package pk.labs.Lab9.beans.impl;

import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;

import java.beans.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class ListaKonsultacji extends java.lang.Object implements Serializable, ConsultationList, VetoableChangeListener {

    private PropertyChangeSupport propertyChangeSupport;
    private final VetoableChangeSupport vetoableChangeSupport = new VetoableChangeSupport(this);

    public List<Consultation> konsultacje;
    public int size;

    public ListaKonsultacji() {
        propertyChangeSupport = new PropertyChangeSupport(this);
        konsultacje = new LinkedList<Consultation>();
    }

    public ListaKonsultacji(List<Consultation> noweKonsultacje) {
        propertyChangeSupport = new PropertyChangeSupport(this);
        this.konsultacje = noweKonsultacje;
    }

    public ListaKonsultacji(LinkedList<Consultation> noweKonsultacje){
        propertyChangeSupport = new PropertyChangeSupport(this);
        this.konsultacje = new LinkedList<Consultation>();
        try{
            for (Consultation k: noweKonsultacje){
                this.addConsultation(k);
            }
        } catch (PropertyVetoException ex){

        }
    }

    @Override
    public int getSize() {
        return this.konsultacje.size();
    }

    @Override
    public Consultation[] getConsultation() {
        return (konsultacje.toArray(new Consultation[konsultacje.size()]));
    }
    public void setConsultation(List consultations) {
        this.konsultacje = consultations;
    }

    @Override
    public Consultation getConsultation(int index) {
        return this.getConsultation()[index];
    }

    @Override
    public void addConsultation(Consultation consultation) throws PropertyVetoException {
        try{
            int oldSize = this.getSize();
            if (oldSize > 0) {
                vetoableChangeSupport.addVetoableChangeListener((VetoableChangeListener)consultation);
                vetoableChangeSupport.fireVetoableChange("consultation", konsultacje.get(getSize() - 1), consultation);
            }

            ((Konsultacja)consultation).addVetoableChangeListener(this);
            konsultacje.add(consultation);

            propertyChangeSupport.firePropertyChange("consultation", oldSize, oldSize + 1);

        } catch(PropertyVetoException e) {
            throw e;
        }
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void addVetoableChangeListener(VetoableChangeListener listener) {
        vetoableChangeSupport.addVetoableChangeListener(listener);
    }

    public void removeVetoableChangeListener(VetoableChangeListener listener) {
        vetoableChangeSupport.removeVetoableChangeListener(listener);
    }

    @Override
    public void vetoableChange(PropertyChangeEvent event) throws PropertyVetoException {
        Konsultacja oldConsultation = (Konsultacja)event.getOldValue();

        int minutes = (Integer)event.getNewValue();

        long consultationStart = oldConsultation.getTerm().getBegin().getTime();

        long endAfterProlong = oldConsultation.getTerm().getEnd().getTime() + minutes * 60000;

        for (Consultation consultation : konsultacje) {
            long start = ((Konsultacja)consultation).getTerm().getBegin().getTime();
            if((consultationStart < start) && (endAfterProlong >= start)) {
                throw new PropertyVetoException("czas trwania", event);
            }
        }
    }
}
